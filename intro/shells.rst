Python shells
====================

python
------------------

* Default **shell** delivered with python installation

ipython
---------------

* easy to install,
* command reverse search **ctrl-r**,
* has additional functionalities,
* colorful

.. hint::

   %timeit # to measure time
   !ping # or whatever command

jupyter-notebook
------------------

* based on **ipython**,
* most often used within **data science** teams,
* additional functions like printing
