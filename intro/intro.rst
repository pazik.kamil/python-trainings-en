Intro
====================

About Python
----------------

* Open Source,
* Name comes from BBC show Flying Circus of Monty Python. Creator is fan of the series,
* Development of **Python** (interpreter) started in 1989,
* Guido van Rossum - was creator and dictator for python. He is like Linus Torvalds for Linux kernel,
* Created to create system tool for very specified Amoeba system (since there were no Coreutils_ and creating new applications using C/Assembler would take long time),

.. _Coreutils: https://en.wikipedia.org/wiki/GNU_Core_Utilities

* Language between C and Shell (bash),
* Python 2.0 – October 2000,
* Python 3.0 - December 2008


Usage
----------------

* DevOps

  * Boto3,
  * Redhat - tools,

    * Installer **Anaconda**,
    * system-config-network-tui,
    * system-config-services,
    * others *system-config-*,
    * Package installers (**yum - python2, dnf - python3**),
    * OpenStack,
    * Ansible ( management of configuration / deployment )

* Data Science / Machine Learning

  * sklearn,
  * Tensorflow,
  * pySpark
    
* Web Development

  * Django,
  * Flask
  

Who's using
------------

- Google – as a main language (at least in the past), next to Java and C++. For processing great amount of data from the users,
- Netflix – for scaling infrastructure, alerts in case of change security settings,
- Instagram (framework Django), Facebook (Framework Tornado),
- Spotify (Big volume of data to process – Luigi),
- Nasa

