Python
====================

Structured programming
-----------------------
* Its possible to write simple scripts

Object programming
-------------------
* Everything is an object


Functional programming
--------------------------

>>> digits = [1, 2, 3, 4, 5]
>>> power_of_two = [2**n for n in digits]
>>> power_of_two
[2, 4, 8, 16, 32]

Dynamic typing
---------------------
* Types are defined during program execution,
* On the one hand - freedom, on the other hand - slower execution,
* No compiling - errors are appearing after execution of line of code not during compilation

.. testcode::

   place  = 43 # int
   place = "near window" # str
   print(place)

The variable will be overwritten (also type will be changed)

.. testoutput::

   near window

Garbage collector
-----------------
* Manages of data cleaning,
* Based on algorithm counting occurrences of **references** to specific object

Passing by reference instead of by value
------------------------------------------------
* As default reference of an object is passed (to safe memory),

