Ide
====================

pyCharm
-------
Paid solution from JetBrains
* A lot of plugins,
* Support of frameworks,
* Integraion with Docker, database, console, cloud solutions

.. image:: pycharm.png
   :alt: pycharm screenshot

Visual Studio Code
------------------
* Free tool,
* You need to install plugins


Eclipse
-------
* Free tool

Spyder
------
* Free toll
* Used in science purposes or data analysis. Niche, replaced by **Jupyter Notebook**

IDLE
----
* Free tool,
* Not recomended,
* Hard development proess
