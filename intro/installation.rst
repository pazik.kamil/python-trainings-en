Installation
====================

Windows
-------
* Installer exe available on the `Python page <https://www.python.org/downloads/windows/>`_


Mac
---
* Can be installed using brew,
* Eventually from page_,
* Best would be using pyenv_

.. _page: https://www.python.org/downloads/mac-osx/
.. _pyenv: https://github.com/pyenv/pyenv

.. hint:: 

   You may use commands like:

   .. code-block:: bash

      pyenv versions # to show all available versions
      pyenv global # to see global configuration
      pyenv install 3.6.9 # to install specific version of python
      pyenv global 3.6.9 # to setup global version of python

   

Linux
-----
* On **debian** or **ubuntu** ``sudo apt-get update && sudo apt-get install python python3-pip python3-venv``
* On **fedora** ``sudo dnf install python`` or specific version ``sudo dnf install python37``

.. hint:: For checking current version we execute this command: ``python --version`` or ``python -V``

Source code
-------------
* Clone of the repository from github_

.. _github: https://github.com/python/cpython


