Python run environment
=========================
**Python** can be launched in couple ways.

Virtual Environment (venv)
---------------------------
* Keeps environment separate,
* Solves problem of dependencies, packages conflicts,
* Helps keeping different **python/libraries** in our projects

.. note:: In order to create virtual environment we do **one of below** commands
   - ``python -m venv <DIR>`` instead *DIR* usually we use *venv* or *env*
   - ``virtualenv <KATALOG>`` ex. ``virtualenv venv`` 

.. note:: In order to use environments we need to execute:
   ``source venv/bin/activate``

.. hint:: In case of **Windows** user is activating env withouth **source** command.
   ``venv\Scripts\activate.bat``

Global environment
-------------------
* All packages are global - there is no separation,
* There might problems with dependencies

Environment in container
--------------------------
* Python available within container, 
* Good in case of testing solutions,
* Integral part of nowadays **CI/CD** environments

Creation of new environment
---------------------------------------

1. ``python -m venv venv``,
2. ``source venv/bin/activate``,

Installation of new packages inside of environment
------------------------------------------------------------------------------

1. *Optional step* ``pip freeze``,
2. ``pip install <package_name>``,
3. ``pip freeze`` Just to verify what has been installed,
4. *Optional step* ``pip freeze > requirements.txt``

Exercise
---------
This exercise will show you typical use case of virtual environments.

 .. attention::

    1. Create new virtual environment,
    2. Activate virtual env,
    3. Look on ``pip freeze``,
    4. Install django, 
    5. Look on ``pip freeze``,
    6. Deactivate virual env,
    7. Remove virtual env,
    8. Start from scratch,
    9. Install notebook

