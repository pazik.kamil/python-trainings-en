Integer number
================

Defining
------------

>>> net_salary = 8000
>>> print(net_salary)
8000

Checking types
----------------
>>> type(net_salary) 
<class 'int'>

Comparing types: string and integer

>>> salary_str = '8000'
>>> salary_str == net_salary
False

Converting types
-------------------
>>> salary_converted = int(salary_str)
>>> salary_converted == net_salary
True

Operations on digits
--------------------

After salary increase we get 5% more money

>>> net_salary = net_salary*1.05
>>> print(net_salary)
8400.0

>>> type(net_salary) == float
True

>>> type(net_salary)
<class 'float'>

Adding
--------

- we received 200 PLN monthly bonus 

>>> net_salary += 200
>>> net_salary = int(net_salary)
>>> print(net_salary)
8600

Integer division
-------------------

- We want to calculate net income **per person** in 3 persons family,
- We want to round the income to the 2nd decimal place (rounding)

>>> print(round(net_salary / 3, 2))
2866.67

>>> net_salary // 3
2866

>>> round(net_salary / 3)
2867

As we see we lost precision. Values after commas has been ignored


Modulo division
-------------------

- We want to check if our salary is **even** *(divisible by two)*

>>> print(net_salary % 2)
0

There is no **reminder** so it's **even** number
