Lists
==========================

Defining
------------

>>> salary_list = [4000, 5000, 3000, 8000]
>>> print(salary_list)
[4000, 5000, 3000, 8000]

Checking type
----------------

>>> type(salary_list)
<class 'list'>

Operation on lists
-------------------

* Checking list type

>>> len(salary_list)
4

* Checking occurrences withing list

>>> 3000 in salary_list
True

* Adding element into list

.. testcode::

   salary_list.append(12000)
   print(salary_list)

.. testoutput::

   [4000, 5000, 3000, 8000, 12000]

* Adding element into concrete place into list

.. testcode::

   salary_list.insert(1, 4500)
   print(salary_list)

.. testoutput::

   [4000, 4500, 5000, 3000, 8000, 12000]

* List sorting 

.. testcode::

   salary_list = sorted(salary_list)
   print(salary_list)

.. testoutput::

   [3000, 4000, 4500, 5000, 8000, 12000] 


.. testcode::

   print(sorted(salary_list, reverse=True))

.. testoutput::

   [12000, 8000, 5000, 4500, 4000, 3000]

* List sorting **(in place)**

.. testcode::

   salary_list.extend([2800, 15000])
   salary_list.sort()
   print(salary_list)

.. testoutput::

   [2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000]

* Getting last element from the list

>>> retreived = salary_list.pop() 
>>> print(retreived)
15000

Again we put element to the list

>>> salary_list.append(retreived)
>>> print(salary_list)
[2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000]

Let's put element which is incorrect (negative salary)

>>> salary_list.append(-300)
>>> print(salary_list)
[2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000, -300]

This negative is not needed - let`s remove it:

>>> del salary_list[-1]
>>> print(salary_list)
[2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000]

* Iterating over the list

.. testcode::

   for salary in salary_list:
       print(salary)

.. testoutput::

   2800
   3000
   4000
   4500
   5000
   8000
   12000
   15000

Type conversion
------------------
- into **tuple**

.. testcode::

   salary_tuple = tuple(salary_list)
   print(salary_tuple)

.. testoutput::

   (2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000)

- into **set**

>>> set_plac = set(salary_list)
>>> print(set_plac)  # doctest: +SKIP

Exercises part 1
-------------------

#. Create list containing:

    * Audi

    * Bmw

    * Mercedes

    * Mazda

#. Replace **Audi** with **Mazda**
#. Remove last car
#. Print last car on the list

Exercised part 2
--------------------

#.  Create list of temperatures ``[-5, -4, 0, -3, -2, 9, 10]``,
#.  Sort descending - **in place**,
#.  Sort ascending - **not in place**
