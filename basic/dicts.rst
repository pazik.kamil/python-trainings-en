Dictionaries
==========================
Type of data **key - value**

Defining
------------

>>> workers = {1: 'Adam', 3: 'Tomasz', 4: 'Kasia'}
>>> print(workers) # doctest: +SKIP

Checking type
----------------

>>> type(workers)
<class 'dict'>

Operations on dictionary
--------------------------

* Checking length of dictionary

>>> len(workers)
3

* Checking element occurrences

>>> 3000 in workers
False

>>> 1 in workers
True

Employee with ``id 1`` exists inside of dictionary

* Adding element to the list

>>> workers[15] = "Marek"
>>> print(workers) # doctest: +SKIP
>>> print(len(workers))
4

Exercises
----------


#. Create dictionary with capitals of:

  * France,
  * Germany,
  * Poland,
  * Czech republic

#. Get capital of **Uk** - in case of not having capital within dictionary print ``"unknown capital``
#. Remove capital of Czech republic from dicionary,

.. hint::

   Look for the method which is giving you some text in case of not having specific key inside of dict

