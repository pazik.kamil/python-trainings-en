Float numbers
==========================

Defining
------------

>>> net_salary = 8000.63
>>> print(net_salary)
8000.63

Checking type
----------------
>>> type(net_salary) 
<class 'float'>

Type conversion
---------------
>>> salary_converted = int(net_salary)
>>> salary_converted == net_salary
False

Interesting facts
----------------------

>>> print(0.1 + 0.2)
0.30000000000000004

.. hint::

   Answer on the page_.

   .. _page: https://0.30000000000000004.com/

   `Additional wiki page <https://pl.wikipedia.org/wiki/IEEE_754>`__ about **IEEE 754**.

   .. code-block:: python

      import decimal 

      ctx = decimal.getcontext()
      print(ctx)

      a = decimal.Decimal(0.2)
      b = decimal.Decimal(0.1)

      ctx.prec = 6
      print(a + b)

Exercise
------------
#. Calculate sum of 123, 321, 675 and print result on the screen,
#. Check if sum is multiples of the number **5**,
#. Calculate **income tax** (tax rate is 19%) user is giving the amount **(input)**. Assuming tax free allowance is 5000,
#. Calculate **area** of circle (with given **radius** by the user)

.. hint::

   Use **input** function.
   You may also import additional module - search for it using **google**
