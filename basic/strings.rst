Strings
====================

Printing strings
--------------------------

>>> print('Hello World!')
Hello World!

Or using double **quotation** mark

>>> print("Hello World!")
Hello World!


.. note::

   **Single ' quotation** and **Double " quotation** works almost same.
   But if you want to put ``'`` into quotation mark you need to mix quotations characters ex.

>>> print("It's a nice day")
It's a nice day

But if you use ``'`` twice you will see syntax exception

>>> print('It's a nice day')
Traceback (most recent call last):
SyntaxError: invalid syntax

Defining strings
------------------


.. testcode::

   txt = "Hello World!"
   print(txt)

.. testoutput::

   Hello World! 

Checking types
----------------
>>> type(txt)
<class 'str'>

Checking length of string
---------------------------
>>> len(txt)
12


Printing special characters
-------------------------------
* New line special character

>>> print("Hello\nWorld!")
Hello
World!

* Tabulator character

>>> print("Hello\tWorld!") # doctest: +SKIP

String concatenation
---------------------

>>> print('Hello ' +  'attendee') 
Hello attendee

String concatenation - format
------------------------------

>>> print('Hello {}, have a great day'.format('Tomasz'))
Hello Tomasz, have a great day


Different representations - format
----------------------------------------

>>> '{:s}'.format('Some text') # in case of digit - exception
'Some text'

>>> '{:s}'.format(4) # in case of digit - exception
Traceback (most recent call last):
ValueError: Unknown format code 's' for object of type 'int


.. testcode::

   class Data:
       """Simple Data class"""

       def __init__(self, value):
           self.value = value

       def __str__(self):
           return '{}'.format(self.value)

       def __repr__(self):
           return '<{} object with value: {}>'.format(self.__class__.__name__, self.value)


   print("{0!s}".format(Data(54), Data(54)))
   print("{0!r}".format(Data(54), Data(54)))
   print("{obj!s}".format(obj=Data(41)))
   print("{obj!r}".format(obj=Data(41)))

.. testoutput::

   54   
   <Data object with value: 54>
   41
   <Data object with value: 41>

>>> '{:>10}'.format('test')
'      test'

>>> '{:10}'.format('test')
'test      '

>>> '{:^10}'.format('test')
'   test   '

Functions available on srings
------------------------------

>>> 'Hello'.endswith('o')
True

>>> 'Hello'[-1] == 'o'
True

Substrings
--------------------------
>>> 'Hello'[-1]
'o'

>>> 'Hello'[0:6:2]
'Hlo'

.. testcode::

  names = 'Marta, Kasia, Monika, Tomek, Przemek, Janek, Marta, Malgosia'
  
  print(names.count('Ma'))

In result we receive number of occurrences

.. testoutput::

  3

Below we find an index of string.

>>> names.find('Kasia')
7

.. hint::

   Letter **'K'** is at 8th position, which means index no. 7 (countring from 0)

Splitting strings
--------------------------------------

>>> names.split(',')
['Marta', ' Kasia', ' Monika', ' Tomek', ' Przemek', ' Janek', ' Marta', ' Malgosia']

We received list of strings

Operations of strings
------------------------

>>> names = names.replace("Janek", "Adam")
>>> print(names)
Marta, Kasia, Monika, Tomek, Przemek, Adam, Marta, Malgosia

Checking if string is a digit
------------------------------

>>> names.isdigit()
False

>>> temperature = "34"
>>> print(temperature.isdigit())
True

String as uppercase
----------------------

>>> print(names.upper())
MARTA, KASIA, MONIKA, TOMEK, PRZEMEK, ADAM, MARTA, MALGOSIA

String as lowercase
--------------------

>>> print(names.lower())
marta, kasia, monika, tomek, przemek, adam, marta, malgosia

Exercise
-----------
1 Create program writing your name and surname 
2 Print fallowing statement: "Test characters: ', /, " "
3 Create two attendees of this training (give them names which you like) *(both attendees are seperate variables)*

  * first_attendee,
  * second_attendee


4 Exchange places of attendees - ``first_attendee`` should have ``second_attendee`` content and other way round
  
  * Print attendees,
  * Is it possible to change places in different way ?

5 Let user put his name using keyboard **(Use google)**
