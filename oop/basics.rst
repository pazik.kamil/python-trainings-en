Object oriented programming 
====================================

Class creation 
----------------

.. testcode::

   class Human:
       pass

   adam = Human()

Constructor
------------

* is explaining what values should be assigned during creation of an instance

.. testcode::

   class Human:
       def __init__(self, name):
           self.name = name

   eve = Human('Eve')
   print(eve.name)

.. testoutput::

   Eve
      
Self
-------

* ``self`` is like ``this`` in other languages like java/c#,
* Its pointing to our instance/object,
* It could be named different but ``self`` is convention


Instance vs class
-------------------

* class Human is a class (just concept / definition),
* ``adam = Human('Adam')`` is creation of an object/instance,
* adam is an object (concrete - creation of concept)

.. testcode::

   class Human:

       def __init__(self, name):
           self.name = name

   adam = Human('Adam')
   print(adam.name)

.. testoutput::

   Adam

Class variables
---------------

* variables which stay the same across different objects

.. testcode::

   class Human:
       species = 'homo-sapiens'

       def __init__(self, name):
           self.name = name

   print(Human.species)
   adam = Human('Adam')
   print(adam.name)
   print(adam.species)

.. testoutput::

   homo-sapiens
   Adam
   homo-sapiens

Special methods
----------------

+-------------+-----------------+-----------------+------------------------------------------+
| Method      | Parameters      | Operator        | Meaning                                  |
+=============+=================+=================+==========================================+
| add         | (self, other)   | \+              | Adding objects                           |
+-------------+-----------------+-----------------+------------------------------------------+
| sub         | (self, other)   | -               | Subtracting objects                      |
+-------------+-----------------+-----------------+------------------------------------------+
| len         | (self)          | len             | Getting length of an object              |
+-------------+-----------------+-----------------+------------------------------------------+
| contains    | (self, other)   | in              | Check if in                              |
+-------------+-----------------+-----------------+------------------------------------------+
| str         | (self)          | str             | Convert object to str                    |
+-------------+-----------------+-----------------+------------------------------------------+
| repr        | (self)          | repr            | Get representation of object             |
+-------------+-----------------+-----------------+------------------------------------------+


Composition
-----------



Aggregation
-------------


Composition vs Aggregation
----------------------------

+-------------+--------------+--------------+
| Composition | Composition  | Aggregation  |
+=============+==============+==============+
| Creation    | Inside       | Outside      |
+-------------+--------------+--------------+
| Deletion    | With main ob | Independent  |
+-------------+--------------+--------------+



Example
--------

.. testcode::

   import random

   class Car:
       colors = ['red', 'blue', 'black']
       
       def __init__(self, brand='', color=None):
           self.brand = brand
           
           if not color:
               self.color = random.choice(self.colors)
               
       def __repr__(self):
           return "<{class_name} of brand: {brand} and color: {color}>".format(
               class_name=self.__class__.__name__,
               brand=self.brand,
               color=self.color
           )
       
       def __str__(self):
           return "{color} {brand} car".format(color=self.color, brand=self.brand)

   bmw = Car('Bmw')
   repr(bmw)
   str(bmw)


Checking types
---------------


.. note::

   check this code here_

   .. _here: https://github.com/python/cpython/blob/b731fc521cf78e53268e35777d836ca80e7ab305/Lib/os.py#L972

Exercises - part 1
---------------------------

#. Create class **mechanical_vehicle**, which is inheriting after **vehicles**,
#. When we create **mechanical vehicle** we need to know its unique id - its called **VIN** number,
#. Add fields:

  * Fuel consumption per 100 km
  * Add properties (property decorator) - miles left

#. Add method ``go(how_far)`` - this should change ``fuel_amount`` state and ``milage`` state,

Exercises - part 2 
---------------------------

#. Create class **Server** which got:

  * Name,
  * Ip,
  * Create ``ping`` method (use ``os`` – execute ping command),
  * Change **representation** and **conversion to string** methods,
  * Store history of ping - date and status,
  * Create list of hosts for **pinging** [ ‘127.0.0.1’, …..],
  * Iterate over the list and print message for hosts if they are **pingable**

.. hint::

  * Use ``os`` library

.. hint::

   Library __pathlib__ (std). Class PurePath_:

   .. _PurePath: https://github.com/python/cpython/blob/35d9c37e271c35b87d64cc7422600e573f3ee244/Lib/pathlib.py#L1007

.. hint::

   Library ldap3_:

   .. _ldap3: https://github.com/cannatag/ldap3/blob/2d19835dfe55247647d3334212efd13dd0899451/ldap3/core/server.py#L52

Exercises - part 2 
---------------------------

#. Create class **Cluster** which got:

  * Location,
  * Name

#. Its also to do ``len`` and add ``+`` on ``Cluster`` object
