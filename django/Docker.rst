Docker
===========

What for
---------

* local development,
* deployment

How it works
------------

* cgroups,
* native on Linux,
* non native on Mac(Hyperkit), Windows

Definition
-----------

.. code-block:: docker

   FROM python:3.6

   MANTAINER kamil pazik

   ENV API_HOME /opt/api

   RUN apt-get update
   RUN apt-get install -y vim

   RUN pip install --upgrade pip

   WORKDIR $API_HOME


Exercise - part 1
----------------------

  * create ``Dockerfile`` and install there a **django** automatically,
  * build the docker image, and tag it as ``django_alpha_image``,
  * print images - how many of them you got in your system,
  * run the container,
  * log into the container and try to ``ping`` some server
