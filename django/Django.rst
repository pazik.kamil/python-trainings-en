Web api - Django
==========================

Creation of virtual env
----------------------------------

.. code-block:: bash

   python -m venv venv
   source venv/bin/activate

Instalation
----------------

.. code-block:: bash

   pip install django
   pip install djangorestframework # remember to add 'rest_framework', to settings.py - INSTALLED_APPS
   pip install django-extensions # remember to add 'django_extensions', to settings.py - INSTALLED_APPS
   pip install markdown       # Markdown support for the browsable API.
   pip install django-filter  # Filtering support - remember to add to settings.py 'django_filters' - INSTALLED_APPS

Creation of dependencies
-----------------------------

.. code-block:: bash

   pip freeze > requirements.txt

Creation of new project
--------------------------

.. code-block:: bash

   django-admin startproject servermonitoring
   cd servermonitoring
   python manage.py migrate
   python manage.py runserver

Create of new app
--------------------------

.. code-block:: bash

  django-admin startapp api

Check in browser
---------------------------

* Chrome/Postman address ``http://127.0.0.1:8000/``
* Curl ``http://127.0.0.1:8000/``

Adjust settings
----------------------

.. tip::

   look on ``manage.py`` ex. using command:
   ``cat manage.py``
   there you got way how django is launched

   change setup in ``settings.py`` 

.. code-block:: bash

   vim servermonitoring/settings.py

.. hint::

   you can do that using vim
   ``vim servermonitoring/settings.py``

   Or directly in **pyCharm**

.. code-block:: bash

   # servermonitoring/settings.py

   INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'django_extensions',
    'rest_framework',
    'api'
   ]

Tests creation
-----------------

.. code-block:: python

   # api/tests.py

   from django.test import TestCase
   from rest_framework import status

   from api.models import Server

   class ServerModelTestCase(TestCase):
       """Server model tests"""

       def setUp(self):
           """Definition of startup values"""

           self.server = Server(address='127.0.0.1')

       def test_model_repr(self):
           self.assertEqual("<Server address: 127.0.0.1>", repr(self.server))

       def test_model_str(self):
           self.assertEqual("Server o adresie: 127.0.0.1", str(self.server))

Test execution
---------------

.. code-block:: bash

   python manage.py test


Model creation
-----------------

.. code-block:: python

   from django.db import models

   class Server(models.Model):
       created = models.DateTimeField(auto_now_add=True)
       location = models.CharField(max_length=100, null=True, blank=True, default='')
       available = models.BooleanField(default=False)
       address = models.GenericIPAddressField()
       admin_contact = models.EmailField(max_length=70, null=True, blank=True)
       admin_phone = models.CharField(max_length=70, null=True, blank=True, default='')

       def __repr__(self):
           return "<{} address: {}>".format(self.__class__.__name__, self.address)

       def __str__(self):
           return "{} o adresie: {}".format(self.__class__.__name__, self.address)

Creation and execution of migations
--------------------------------------

.. code-block:: bash

   python manage.py makemigrations
   python manage.py migrate

Checking of sql migrations code
---------------------------------

.. code-block:: bash

   python manage.py sqlmigrate api 0001

Execution of test after migrations
-----------------------------------

.. code-block:: bash

   python manage.py test

Addint view test
--------------------

.. code-block:: python

   # api/tests.py
   # ............

   from django.test import TestCase
   from rest_framework import status
   from api.models import Server
   from django.urls import reverse

   class ViewServerTestCase(TestCase):
       """Test for server view"""

       def test_create_server(self):
           """We check if we can create server (post)"""

           url = reverse('servers')
           data = {"location": "office", "address": "127.0.0.1"}

           response = self.client.post(url, data, format="json")

           self.assertEqual(response.status_code, status.HTTP_201_CREATED)
           self.assertEqual(len(response.data), 1)

       def test_view_server_list(self):
           """We check if we get proper amount of servers"""

           url = reverse('servers')

           response = self.client.get(url, format="json")
           self.assertEqual(response.status_code, status.HTTP_200_OK)


Serializer
-----------

.. code-block:: bash

   # api/serializers.py

   from rest_framework import serializers

   from .models import Server

   class ServerSerializer(serializers.ModelSerializer):
       class Meta:
           model = Server
           fields = '__all__' # albo fields = ('location', 'address',)


Adding view
--------------------

.. code-block:: bash

   # api/views.py

   from rest_framework.views import APIView
   from rest_framework.response import Response
   from rest_framework import status
   
   from .serializers import ServerSerializer
   from .models import Server
   
   class ServerList(APIView):
       """Lista serwerow"""
   
       serializer_class = ServerSerializer
   
       def get_queryset(self):
           queryset = Server.objects.all()
           location = self.request.query_params.get('location', None)

           if location is not None:
               queryset = queryset.filter(location__icontains=location)
           return queryset

       def get(self, request, format=None):

           servers = self.get_queryset()
           serializer = ServerSerializer(servers, many=True)
           return Response(serializer.data)
   
       def post(self, request, format=None):
           serializer = ServerSerializer(data=request.data)
   
           if serializer.is_valid():
               serializer.save()
               return Response(serializer.data, status=status.HTTP_201_CREATED)
           return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


Add urls
-------------

.. code-block:: python

   from django.contrib import admin
   from django.urls import path

   from api import views

   urlpatterns = [
       path('admin/', admin.site.urls),
       path('servers/', views.ServerList.as_view(), name='servers')
   ]


Methods
----------

* Get

* Post

* Put

* Delete        

HTTP codes
---------

* ``200`` - success. Request correct. Response correct.
* ``400`` - fail **request**. Bad request / problems with authentication.
* ``403`` - access denied,
* ``404`` - no such page,
* ``500`` - internal error. Usually because of developer made mistake.


Requests
--------


Responses
---------------------

Settings
---------



View
-----

Migations
-----------

.. hint::

   To see migrations we can execute
   ``python manage.py showmigrations``

Orm
---

Django extensions
------------------

.. code-block:: python

   python manage.py show_urls
   python manage.py shell_plus # better console/dev server- ipython
   python manage.py runserver_plus # server 

Practice
----------

Zero
____

* Napraw test,
* Dodaj do testu sprawdzanie daty - stworzenia wpisu

First
________

* Create 4 hosts - each in different way

  * Using web page,
  * Using ``request`` from  postman_,

.. _postman: https://www.getpostman.com/
  * Using``python manage.py shell`` lub ``shell_plus``,
  * Using insert to data base

* Create model, which:

  * Would be storing  ``date``


Second
________

* Stwórz endpoint (POST), który:

  * Będzie

Third
______
