docker-compose
================

Why
----

* To manage multiple parts of system
  
  * Databases,
  * Web servers,
  * Other servers

Definition
-----------

.. code-block:: dockerfile

   version: '3'
   services:
     web:
       build: .
       ports:
       - "5000:5000"
       volumes:
       - .:/opt/api

Exercise 1
------------

* create ``docker-compose.yml`` in version 3.5,
* define web service - you should have port 8000 published,
* name container - whatever name you like,
* launch ``docker-compose up``,
* Define ``entrypoint.sh``,
* make ``docker-compose up``
