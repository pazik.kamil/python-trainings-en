Files operations
=============================

Files reading
--------------------

.. code-block:: python

   file = 'file.txt'



Reading line by line
-------------------------


.. code-block:: python

   with open(r'../plik.txt') as file_descriptor:
       lines = file_descriptor.readlines()

Saving
--------------

.. code-block:: python

   with open(r'/tmp/iris.csv', mode='w') as file_descriptor:
       file_descriptor.write('hello')

Context manager
---------------

.. code-block:: python

   with open(r'plik.txt') as file_descriptor:
        for linia in file_descriptor:
            print(linia)
