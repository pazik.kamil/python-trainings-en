Stdlib
=============================

Pickling
--------
Process of serialization of binary data into the file

Dump
----

.. code-block:: python

   hours = ['Tue Mar 29 23:40:17 +0000 2016', 'Tue Mar 29 23:40:19 +0000 2016']

   file_store = open('/Users/kamil/daty.pickle', 'wb') # write/binary
   pickle.dump(hours, file_Store)

Load
----

.. Hint::

   Method ``load`` instead dump

.. Warning::

   In case **Linuxa** there are diffrent paths format than in case of  **Windows**.
   

Exercise
------------

#. Download following file_

.. _file: https://python.variantcore.com/daty.pickle

#. Process dates (strings) into ``datetime`` type

.. hint::

   In order to setup date format look on docs docs_

.. _docs:

  https://docs.python.org/3/library/datetime.html

#. Save pickle after processing
#. Read pickle into different variable - check
