.. python training en documentation master file, created by
   sphinx-quickstart on Fri Sep 13 22:27:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python training [EN]
==============================================

.. toctree::
   :maxdepth: 3 
   :numbered:
   :caption: Contents:



*****
Intro
*****
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: intro
    :caption: Intro

    intro/intro
    intro/installation
    intro/python_env
    intro/ide
    intro/python
    intro/shells

********
Basics
********
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: basic
    :caption: Basic

    basic/strings
    basic/integers
    basic/floats
    basic/lists
    basic/dicts
    basic/sets

**************************
Code flow
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: flowcontrol
    :caption: Controling the flow

    control-flow/ifelse
    control-flow/loops
    control-flow/comprehensions
    control-flow/functions
    control-flow/exceptions
    control-flow/iterators
    control-flow/generators


**************************
Standard library
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: stdlibs
    :caption: Standard library

    stdlibs/pickling
    stdlibs/files
    stdlibs/regex

**************************
Object programming
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: oop
    :caption: Object programming

    oop/basics

**************************
Network
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: network
    :caption: Network

    network/paramiko
    network/smtplib

**************************
Pandas
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: pandas
    :caption: Pandas

    pandas/pandas

**************************
Django
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: django
    :caption: Django

    django/Docker
    django/Docker-compose
    django/Django

*******
Contact
*******
.. toctree::
    :maxdepth: 3
    :name: contact
    :caption: contact

    contact

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
