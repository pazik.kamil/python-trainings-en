Exceptions
==========================

* In case of execution illegal operation,
* In case of resource being unavailable for us - ex. no access rights / not enough memory / servers is unavailable.
  

Syntax Errors
-------------

.. code-block:: python

   >>> while True print('Hello world')
   File "<stdin>", line 1
   while True print('Hello world')
                   ^
   SyntaxError: invalid syntax


Key Errors
----------

.. code-block:: python

   capitals = {"France": "Paris", "Germany":"Berlin", "Poland":"Warsaw", "Check-republic":"Praga"}
   capitals["USA"]

   KeyError: 'USA'

Attribute error
---------------

* If operation not possible to be done

.. code-block:: python

   "Hello Wordl".append('!')


Indentation Error
-----------------

.. code-block:: python

   def testfunc():
   print('Hello ;)')
    print('My name is:')

   File "<ipython-input-4-9cd3c6fb52a1>", line 3
    print('My name is:')
    ^
   IndentationError: unexpected indent

ModuleNotFoundError
--------------------

.. code-block:: python

   import not_existing_module

   ModuleNotFoundError: No module named 'not_existing_module'

Table of exceptions hierarchy in Python_.

.. _Python: https://docs.python.org/3/library/exceptions.html#exception-hierarchy

IndexError
----------

.. code-block:: python

   attendees = ['Kasia', 'Adam', 'Tomek']
   attendees[6]

   IndexError: list index out of range

Exception handling
----------------------

.. testcode::

   for i in range(3, -3, -1):
    try:
        print('Try of division by {}'.format(i))
        3 / i
    except ZeroDivisionError:
        print('Skipping, illegal operation !!!')

    finally:
        print('End of handling')

.. testoutput::

   Try of division by 3
   End of handling
   Try of division by 2
   End of handling
   Try of division by 1
   End of handling
   Try of division by 0
   Skipping, illegal operation !!!
   End of handling
   Try of division by -1
   End of handling
   Try of division by -2
   End of handling

Raising an exception
--------------------

.. code-block:: python

   def generate_report(input_data, outputfile):
       raise NotImplementedError('Function development still in progress')

   NotImplementedError: Function development still in progress

Exercises part 1
------------------

#. You got list of attendees

  * attendees = ["Kasia", "Adam", "Tomek"]

#. Handle the situation when

  * Element no. 5 is gathered,

#. Handle situation when trying to access to capital of **Italy**

   * use ``capitals = {"France": "Paris", "Germany":"Berlin", "Poland":"Warsaw", "Check-republic":"Praga"}``
    



