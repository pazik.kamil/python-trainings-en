Loops
==========================

for loop
-----------


.. testcode::

   for i in range(5):
       print(i)

.. testoutput::

   0
   1
   2
   3
   4

.. testcode::

   for i in range(2, 6):
       print(i)

.. testoutput::

   2
   3
   4
   5

.. testcode::

   for i in range(2, 7, 2):
       print(i)

.. testoutput::

   2
   4
   6

while
-----

.. testcode::

   # i is our "iterator variable"
   i = 0
   while i < 10:
       print(i)
       i += 1

.. testoutput::

   0
   1
   2
   3
   4
   5
   6
   7
   8
   9

.. testcode::

   # i is our "iterator variable"
   i = 6
   while i < 10:
       if i == 7:
           print('Lucky 7')
           i += 1
           continue

       print(i)

       i += 1

.. testoutput::

   6
   Lucky 7
   8
   9


.. testcode::

   # i is our "iterator variable"
   i = 6
   while i < 10:
       if i == 7:
           print('Lucky 7')
           break

       print(i)

       i += 1

.. testoutput::

   6
   Lucky 7

iterate over iterables
-----------------------

.. testcode::

   for car in ['BMW', 'Audi', 'Mercedes']:
       print(car)

.. testoutput::

   BMW
   Audi
   Mercedes


Exercise - part 1
----------------------

#. Create ``dictionary of hosts`` where you store date of connection check and status if it went well

    * You can define list of hosts ex. ``wp.pl, google.com, ing.pl, nonexisting.domain``

.. hint::

   You may get date by using ``datetime``,
   you may also get the data from system using ``os.popen``

Exercise - part 2 
----------------------

  * create list of even numbers from 0 to 100,
  * print this list
