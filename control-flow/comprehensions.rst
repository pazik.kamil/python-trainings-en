List/Dict/Set comprehensions
=============================
Its used for code readability

.. hint::

   At first, its better to create code withouth comprehenstion, later if you got experience you may try to make code with "comprehensions"

.. testcode::

   even_numbers = [element for element in range(2, 21, 2)]
   print(even_numbers) 

.. testoutput::

   [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
   

.. testcode::

   even_numbers2 = [element for element in range(2, 21) if (element % 2) == 0 ]
   print(even_numbers2) 

.. testoutput::

   [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

.. testcode::

   even_numbers3 = [element for element in range(2, 21) if not (element % 2)]
   print(even_numbers3) 

.. testoutput::

   [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

Dict comprehension
------------------

.. testcode::

   data_dict = {'Adam': 'Audi', 'Tomek': 'BMW', 'Kasia': 'Citroen'} # doctest: +SKIP

Set comprehension
-----------------

.. testcode::

   data_set = {i**2 for i in range(5)}
   print(data_set)

.. testoutput::

   {0, 1, 4, 9, 16}

Exercise part 1
-------------------

#. Find 20 numbers divisible by  2 or divisible by 5 (if you don't know how to make list comprehension, create normal list)

Exercise part 2 
-------------------

#. Create mapping (dict comprehension)
  
  * **key** is number, values are letters from the alphabet A-Z,
  * ``{0: 'A', 1: 'B', 2: 'C', 3: 'D', ...}``

.. hint::

   Take a look on ASCII table

   * You can convert number to character,,
   * You can use ``chr()`` function - check **google**

Exercise part 3 
-------------------

#. having **lista dat** in file_

.. _file: https://python.variantcore.com/lista_dat.txt

  * Create list - result of processing file gettting just date (list comprehension).
    * Based on this list - create list, where event occured in 19th second

