If else statements
==========================

* if

.. testcode::

   if True:
       print('True value')

Upper code is returning following text:

.. testoutput::

   True value

.. testcode::

   if False:
       print('False value')

As you there is nothing printed.

Conversion of list into boolean
-------------------------------

.. testcode::

   empty_list = []

   if empty_list:
       print('List with content')
   else:
       print('List empty')

Text above is returning following text:

.. testoutput::

   List empty

.. attention::

   What happened here is implicit conversion of type list into bool

   >>> bool([])
   False

   >>> bool([1, 2, 3])
   True

Checking bool values
--------------------------------

>>> bool(-1)
True

>>> bool(0)
False

>>> bool(124)
True

>>> bool({})
False

.. warning::
   
   Every number but not zero will return ``True``.
   Number -1 if written in binary (U2) got got ones (1) on decimal positions

Checking ranges
--------------------

.. testcode::

   temperature = 18

   if 16 <= temperature < 24:
       print('Temperature good for biking')
   else:
       print('Temperature not appropiate for biking')

This will give us

.. testoutput::

   Temperature good for biking


If we change temperature to **below zero**

.. testcode::

   temperature = -3 

   if 16 <= temperature < 24:
       print('Temperature good for biking')
   elif 3 <= temperature < 16:
       print('Temperature good for walk')
   elif -5 <= temperature < 3:
       print('Temperature good for skiing')
   else:
       print('Don`t know what to do :(')

.. testoutput::

   Temperature good for skiing

Exercises - part 1
--------------------

#. Let user put his age, check if he is adult,
#. Let user put number, check if the value is float or integer

.. hint::

   There are many ways to do that. Find your own ;)

Exercises - part 2 
____________________
#. Create simple BMI calculator,  which will get all values from ``input``. In result it should return status if person is:
  
    * Overweight,
    * Normal,
    * Underweight

Exercises - part 3
__________________
* Use library ``os`` function ``system`` for checking if host is active

  * Depending on a status print proper message,

.. hint::

   Bear in mind that systems got own status codes after execution commands
