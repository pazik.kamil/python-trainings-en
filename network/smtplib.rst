Smtplib
========



Example code
-------------

.. code:: python

   import smtplib

   gmail_user = 'user@gmail.com'
   gmail_password = 'password'
   
   sent_from = gmail_user

   to = ['me@gmail.com', 'pazik.kamil@gmail.com']
   subject = 'Message subject'
   body = "Hey, what's up?\n\n- You"
   
   email_text = """\
   From: {} 
   To: {} 
   Subject: {} 
   
   {} 
   """.format(sent_from, ", ".join(to), subject, body)
   
   try:
       server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
       server.ehlo()
       server.login(gmail_user, gmail_password)
       server.sendmail(sent_from, to, email_text)
       server.close()
   
       print('Email sent!')
   except:
       print('Something is wrong')


Yagmail
--------

.. code:: python

   import yagmail

   yag = yagmail.SMTP('user@gmail.com', 'password')
   
   contents = [
       "This is the body, and here is just text http://somedomain/image.png",
       "You can find an audio file attached.", '/Users/kamil/code/cisco_python/apache_logs.txt'
   ]
   yag.send('pazik.kamil@gmail.com', 'subject', contents)
   
