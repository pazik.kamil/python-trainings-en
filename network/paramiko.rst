Paramiko
=========

Installation
-------------

.. code:: bash

   pip install paramiko


Connection
------------

.. code:: python

   adress = 'ec2-54-93-218-119.eu-central-1.compute.amazonaws.com' # adjust your address
   username = 'username'
   password = 'password'
   
   client = paramiko.SSHClient()
   client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   client.connect(adress, username= username, password=password) # we need to close connection ourself

.. hint::

   You can check if connection is still open using:
   ``lsof -i@ec2-54-93-218-119.eu-central-1.compute.amazonaws.com``

Using sftp
------------

.. code:: python

   sftp = client.open_sftp()

   sftp.listdir('/var/log')
   sftp.get('/var/log/access_log' ,'access_log')

